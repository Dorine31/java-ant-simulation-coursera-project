package ch.epfl.moocprog;

import ch.epfl.moocprog.app.Context;
import ch.epfl.moocprog.config.Config;
import ch.epfl.moocprog.random.UniformDistribution;
import ch.epfl.moocprog.utils.Time;
import ch.epfl.moocprog.utils.Vec2d;

public abstract class Animal extends Positionable {
	private double angle = UniformDistribution.getValue(0, 2*Math.PI);
	private int hitpoints;
	private Time lifespan;
	
	public Animal(ToricPosition position, int hitpoints, Time lifespan) {
		super(position);
		this.setPosition(position);
		this.hitpoints = hitpoints;
		this.lifespan = lifespan;
	}
	public Animal(ToricPosition position) {
		super(position);
		this.hitpoints = 0;
		this.lifespan = Time.ZERO;
	}
	public final double getDirection() {
		return this.angle;
	}
	protected final void setDirection(double angle) {
		this.angle = angle;
	}
	public final int getHitpoints() {
		return this.hitpoints;
	}

	protected int setHitpoints(int hitpoints) {
		// TODO Auto-generated method stub
		this.hitpoints = hitpoints;
		return this.hitpoints;
	}
	public final Time getLifespan() {
		return this.lifespan;
	}
	protected Time setLifespan(Time lifespan) {
		this.lifespan = lifespan;
		return this.lifespan;
	}
	public abstract double getSpeed();
	
	protected final boolean isDead() {
		if (this.lifespan == Time.ZERO || this.hitpoints <= 0) {
			return true;
		}
		return false;
	}
	public String toString() {
		return super.getPosition() + "\n" + "Speed : " + getSpeed() + "\n" + "HitPoints : " + getHitpoints() + "\n" + "LifeSpan : " +getLifespan();
	}
	
	public abstract void accept(AnimalVisitor visitor, RenderingMedia s);

	public void update(AnimalEnvironmentView env, Time dt) {
		System.out.println("test dans update");
		double ANIMAL_LIFESPAN_DECREASE_FACTOR = Context.getConfig().getDouble(Config.ANIMAL_LIFESPAN_DECREASE_FACTOR);
		System.out.println("animal " + ANIMAL_LIFESPAN_DECREASE_FACTOR);
		this.lifespan = this.lifespan.minus(dt.times(ANIMAL_LIFESPAN_DECREASE_FACTOR));
		System.out.println("this.lifespan minus " + this.lifespan);
		if (Animal.this.isDead() == false) {
			Animal.this.move(dt);
		}
	}
	protected void move(Time dt) {
		Vec2d moved = Vec2d.fromAngle(this.angle).scalarProduct(dt.toSeconds()*getSpeed());
		ToricPosition newPosition = getPosition().add(moved);
		setPosition(newPosition);
	}
}
