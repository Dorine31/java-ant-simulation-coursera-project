package ch.epfl.moocprog;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import ch.epfl.moocprog.app.Context;
import ch.epfl.moocprog.gfx.EnvironmentRenderer;
import ch.epfl.moocprog.utils.*;

import static ch.epfl.moocprog.config.Config.*;

public final class Environment implements FoodGeneratorEnvironmentView, AnimalEnvironmentView {
	private FoodGenerator foodGenerator;
	private List<Food> foods;
	private List<Animal> animals;
	private Animal animal;
	
	public Environment() {
		this.foodGenerator = new FoodGenerator();
		this.foods = new LinkedList<Food>();
		this.animals = new LinkedList<Animal>();
	}
	public void addFood(Food food) {
		if (food == null) {
			throw new IllegalArgumentException();
		} else {
			this.foods.add(food);
		}
	}
	public void addAnimal(Animal animal) {
		if (animal == null) {
			throw new IllegalArgumentException();
		} else {
			this.animals.add(animal);
		}
	}
	public void addAnthill(Anthill anthill) {
		//
	}
	public List<Double> getFoodQuantities() {
		List<Double> quantities = new ArrayList<Double>();
		for (Food food : this.foods) {
			 quantities.add(food.getQuantity());
		}
		return quantities;
	}
	public List<ToricPosition> getAnimalsPosition(){
		List<ToricPosition> positions = new ArrayList<ToricPosition>();
		for (Animal animal : this.animals) {
			positions.add(animal.getPosition());
		}
		return positions;
	}
	// update the environment (add food, add animals)
	public void update(Time dt) {
		this.foodGenerator.update(Environment.this, dt);
		this.animals.removeIf(animal -> animal.getLifespan() == Time.ZERO);
		this.animals.forEach(animal -> animal.update(Environment.this, dt));
		// this.animal.update(Environment.this, dt);
		this.foods.removeIf(food -> food.getQuantity() <= 0);

	}
	
	// graphic view
	public void renderEntities(EnvironmentRenderer environmentRenderer) {
		this.foods.forEach(environmentRenderer::renderFood); //applique renderFood() à chq elmt de la collection
		this.animals.forEach(environmentRenderer::renderAnimal);
	}

	public int getWidth() {
		return Context.getConfig().getInt(WORLD_WIDTH);
	}
	public int getHeight() {
		return Context.getConfig().getInt(WORLD_HEIGHT);
	}
}
