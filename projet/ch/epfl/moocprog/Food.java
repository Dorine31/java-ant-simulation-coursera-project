package ch.epfl.moocprog;

public final class Food extends Positionable {
	private double quantity;
	
	public Food (ToricPosition position, double quantity) {
		super(position);
		if (quantity < 0) {
			this.quantity = 0.0;
		} else {
			this.quantity = quantity;
		}
	}
	public double getQuantity() {
		return this.quantity;
	}
	public double takeQuantity(double quantity) {
		if (quantity < 0) {
			throw new IllegalArgumentException();
		}
		if (this.quantity >= quantity){
			this.quantity -= quantity;
			return quantity;
		} else {
			this.quantity -= this.quantity;
			return this.quantity;
		}
	}
	@Override
	public String toString() {
		return super.toString() + "\n "+" Quantity : "+ getQuantity();
	}
	
}
