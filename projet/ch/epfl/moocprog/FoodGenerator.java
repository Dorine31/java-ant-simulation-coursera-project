package ch.epfl.moocprog;

import ch.epfl.moocprog.app.Context;
import ch.epfl.moocprog.config.Config;
import ch.epfl.moocprog.utils.Time;
import ch.epfl.moocprog.random.UniformDistribution;
import ch.epfl.moocprog.random.NormalDistribution;

public final class FoodGenerator {
	private Time dt;
	
	public FoodGenerator() {
		this.dt = Time.ZERO;
	}
			
	public void update(FoodGeneratorEnvironmentView env, Time dt) {
		Time FOOD_GENERATOR_DELAY = Context.getConfig().getTime(Config.FOOD_GENERATOR_DELAY);
		double taillex = Context.getConfig().getInt(Config.WORLD_WIDTH);
		double tailley = Context.getConfig().getInt(Config.WORLD_HEIGHT);
		double quantity_min = Context.getConfig().getDouble(Config.NEW_FOOD_QUANTITY_MIN);
		double quantity_max = Context.getConfig().getDouble(Config.NEW_FOOD_QUANTITY_MAX);
		ToricPosition position = new ToricPosition(NormalDistribution.getValue(taillex / 2, taillex * taillex / 16.0),
			NormalDistribution.getValue(tailley / 2, tailley * tailley / 16.0));
		double quantity = UniformDistribution.getValue(quantity_min, quantity_max);
		System.out.println("this.dt 0 " + this.dt);
		this.dt = this.dt.plus(dt);
		System.out.println("this.dt avant boucle " + this.dt);
		while (this.dt.compareTo(FOOD_GENERATOR_DELAY) >= 0) {
			
			Food food = new Food(position, quantity);
			System.out.println("this.dt dans boucle avant minus " + this.dt);
			env.addFood(food);
			this.dt = this.dt.minus(FOOD_GENERATOR_DELAY);
			System.out.println("this.dt fin de boucle " + this.dt);
		}
	}
}
