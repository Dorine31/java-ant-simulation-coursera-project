package ch.epfl.moocprog;

import ch.epfl.moocprog.app.Context;
import ch.epfl.moocprog.config.Config;

public final class Termite extends Animal {
	public Termite(ToricPosition position) {
		super(position);
		super.setHitpoints(Context.getConfig().getInt(Config.TERMITE_HP));
		super.setLifespan(Context.getConfig().getTime(Config.TERMITE_LIFESPAN));
	}
	public void accept(AnimalVisitor visitor, RenderingMedia s) {
		visitor.visit(this, s);
	}
	public double getSpeed() {
		return Context.getConfig().getDouble(Config.TERMITE_SPEED);
	}
}
