package ch.epfl.moocprog;

import ch.epfl.moocprog.utils.Vec2d;

import static ch.epfl.moocprog.app.Context.getConfig;
import static ch.epfl.moocprog.config.Config.*;

public final class ToricPosition {
	private final Vec2d position;
	
	/** Constructeur par defaut, initialise position 
	 * @return **/
	public ToricPosition() {
		this.position = new Vec2d(0.0, 0.0);
	};

	/** Constructeur initialise un Vec2d à partir de coordonnées **/
	public ToricPosition(double x, double y) {
		this.position = clampedPosition(x, y);
	};
	
	/** Constructeur initialise un Vec2d à partir d'un Vec2d **/
	public ToricPosition(Vec2d vec) {
		this.position = clampedPosition(vec.getX(), vec.getY());
	};

	
	/** méthode qui prend en paramètre x et y 
	 * et renvoie un objet Vec2d projeté dans un monde torique **/
	private static Vec2d clampedPosition(double x, double y) {
		int world_width = getConfig().getInt(WORLD_WIDTH);
		int world_height = getConfig().getInt(WORLD_HEIGHT);
		while(x < 0) {
			x = x + world_width;
		}
		while(x >= world_width) {
			x = x - world_width;
		}
		while(y < 0) {
			y = y + world_height;
		}
		while(y >= world_height) {
			y = y - world_height;
		}
		return new Vec2d(x, y);
	};
	
	// methodes add
	public ToricPosition add(ToricPosition that) {
		return new ToricPosition(this.position.getX() + that.position.getX(), this.position.getY() + that.position.getY());
	};
	public ToricPosition add(Vec2d vec) {
		return new ToricPosition(this.position.getX() + vec.getX(), this.position.getY() + vec.getY());
	};
	
	public Vec2d toVec2d() {
		System.out.println("tovec2d" + this.position);
		return this.position;
	}
	public Vec2d toricVector(ToricPosition that) {
		int world_width = getConfig().getInt(WORLD_WIDTH);
		int world_height = getConfig().getInt(WORLD_HEIGHT);
		
		Vec2d v2 = new Vec2d(0, world_height);
		Vec2d v3 = new Vec2d(0, - world_height);
		Vec2d v4 = new Vec2d(world_width, 0);
		Vec2d v5 = new Vec2d(- world_width, 0);
		Vec2d v6 = new Vec2d( world_width, world_height);
		Vec2d v7 = new Vec2d(- world_width, - world_height);
		Vec2d v8 = new Vec2d(world_width, - world_height);
		Vec2d v9 = new Vec2d(- world_width,  world_height);
		Vec2d vec1 = new Vec2d(that.position.getX(), that.position.getY());
		Vec2d vec2 = vec1.add(v2);
		Vec2d vec3 = vec1.add(v3); 
		Vec2d vec4 = vec1.add(v4); 
		Vec2d vec5 = vec1.add(v5); 
		Vec2d vec6 = vec1.add(v6); 
		Vec2d vec7 = vec1.add(v7); 
		Vec2d vec8 = vec1.add(v8); 
		Vec2d vec9 = vec1.add(v9); 
		double[] dis = {
			this.position.distance(vec1),
			this.position.distance(vec2),
			this.position.distance(vec3),
			this.position.distance(vec4),
			this.position.distance(vec5),
			this.position.distance(vec6),
			this.position.distance(vec7),
			this.position.distance(vec8),
			this.position.distance(vec9)
		};
		Vec2d vecs[] = {vec1, vec2, vec3, vec4, vec5, vec6, vec7, vec8, vec9};
		int res = 0;
		double min = dis[0];
		int i;
		for (i = 0; i < (dis.length) ; i++) {
			if (min > dis[i]) {
				min = dis[i];
				res = i;
			}
		};

		return (vecs[res].minus(this.position));		
	}
	public double toricDistance(ToricPosition that) {
		return toricVector(that).length();
	}
	public String toString() {
		if (this.position != null)
		    return String.format("Position : %.1f, %.1f", this.position.getX(), this.position.getY());
	    return "0.0 , 0.0";
	}
}
